<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\SaleMap;
use Cadix\SuperOfficeApi\Maps\SaleValidatorMap;

/**
 * Class Sale.
 *
 * @property int    $contactId
 * @property string $name
 */
class Sale extends Model
{
    protected string $model = 'Sale';

    /**
     * Gets a SaleEntity object.
     *
     * @param  int         $id
     * @return object|null
     */
    public function find(int $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;
        $response = parent::get();

        $map = new SaleMap();

        return (object) $map((array) $response);
    }

    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();
        $map = new SaleMap();

        return (array) $map($response['value']);
    }

    /**
     * @return array|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;
        $response = parent::get();

        $map = new SaleMap();

        return ! $response ? $response : (array) $map($response['value']);
    }

    public function create(array $attributes): object
    {
        $map = new SaleValidatorMap();
        $contact = (array)$map($attributes);

        if (! $this->validate($contact)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model;

        return (object)$this->post($contact);
    }

    /**
     * Set default values into a new SaleEntity
     * https://community.superoffice.com/documentation/sdk/SO.NetServer.Web.Services/html/v1SaleEntity_DefaultSaleEntity.htm
     *
     * @return object
     */
    public function default(): object
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/default';

        $response = parent::get();

        $map = new SaleMap();

        return (object) $map((array) $response);
    }

    /**
     * Updates the existing SaleEntity.
     *
     * @param  int    $id
     * @param  array  $attributes
     * @return object
     */
    public function update(int $id, array $attributes): object
    {
        $map = new SaleValidatorMap();
        $contact = (array)$map($attributes);

        if (! $this->validate($contact)) {
            throw new Exception('Invalid attributes');
        }

        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return (object)$this->put($contact);
    }

    public function validate(array $contact): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/Validate';
        $this->client->params = null; // Reset or filters could be added

        $validated = $this->post($contact);

        return is_null($validated) || (is_array($validated) && count($validated) === 0);
    }

    public function delete(int $id): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return parent::destroy($id);
    }
}
