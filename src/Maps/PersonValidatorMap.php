<?php

namespace Cadix\SuperOfficeApi\Maps;

class PersonValidatorMap extends Mapper
{
    public function map(object $toMap): object
    {
        $person = [
            'Firstname'  => $toMap->first_name ?? null,
            'MiddleName' => $toMap->insertion ?? null,
            'Lastname'   => $toMap->last_name ?? null,
            'Mrmrs'      => $toMap->salutation ?? null,
        ];

        if (! empty($toMap->person_id)) {
            $person['PersonId'] = $toMap->person_id ?? null;
        }

        if (! empty($toMap->email)) {
            $person['Emails'] = [
                [
                    'Value' => $toMap->email ?? null,
                ],
            ];
        }

        if (! empty($toMap->person_number) || ! empty($toMap->relation_number)) {
            $person['PersonNumber'] = $toMap->person_number ?? $toMap->relation_number ?? null;
        }

        if (! empty($toMap->phone) || ! empty($toMap->mobile)) {
            $person['MobilePhones'] = [
                [
                    'Value' => $toMap->phone ?? $toMap->mobile ?? null,
                ],
            ];
        }

        if (! empty($toMap->contact_id)) {
            $person['Contact'] = [
                'ContactId' => $toMap->contact_id ?? null,
            ];
        }

        if (! empty($toMap->country) || ! empty($toMap->domain_name)) {
            $person['Country'] = [
                'Name' => $this->countryCode($toMap->country ?? $toMap->domain_name ?? null),
            ];
        }

        if (! empty($toMap->city) || ! empty($toMap->postcode) || ! empty($toMap->zipcode) || ! empty($toMap->street)) {
            $person['Address']['Street'] = [
                'City'     => $toMap->city ?? null,
                'Zipcode'  => $toMap->postcode ?? $toMap->zipcode ?? null,
                'Address1' => $toMap->street ?? null,
            ];
        }

        if (! empty($toMap->associate_id)) {
            $person['Associate']['AssociateId'] = $toMap->associate_id ?? null;
        }

        if (! empty($toMap->business_id)) {
            $person['Business']['Id'] = $toMap->business_id ?? null;
        }

        if (! empty($toMap->category_id)) {
            $person['Category']['Id'] = $toMap->category_id ?? null;
        }

        return (object) $person;
    }
}
