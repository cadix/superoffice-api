<?php

namespace Cadix\SuperOfficeApi\Maps;

class ContactValidatorMap extends Mapper
{
    public function map(object $toMap): object
    {
        $map = [
            'Name'       => $toMap->name ?? null,
            'Department' => $toMap->department ?? null,
            'OrgNr'      => $toMap->orgnr ?? $toMap->vat_number ?? null,
            'Number2'    => $toMap->relation_number ?? null,
            'Country'    => [
                'Name' => $this->countryCode($toMap->country) ?? null,
            ],
            'Address' => [
                'Postal' => [
                    'Address1' => $toMap->street ?? null,
                    'Zipcode'  => $toMap->postcode ?? $toMap->zipcode ?? null,
                    'City'     => $toMap->city ?? null,
                ],
            ],
        ];

        if (! empty($toMap->contact_id)) {
            $map['ContactId'] = $toMap->contact_id ?? null;
        }

        if (! empty($toMap->person_id)) {
            $map['Associate']['PersonId'] = $toMap->person_id ?? null;
        }

        if (! empty($toMap->associate_id)) {
            $map['Associate']['AssociateId'] = $toMap->associate_id ?? null;
        }

        if (! empty($toMap->business_id)) {
            $map['Business']['Id'] = $toMap->business_id ?? null;
        }

        if (! empty($toMap->category_id)) {
            $map['Category']['Id'] = $toMap->category_id ?? null;
        }

        /*
         * TODO this only returns 500 server errors
        if (! empty($toMap->phone)) {
            $map['Phones'][0]['Telephone'] = $toMap->phone ?? null;
        }
        */

        return (object) $map;
    }
}
