<?php

namespace Cadix\SuperOfficeApi\Maps;

class DocumentMap extends Mapper
{
    public function map(object $toMap): object
    {
        return (object) [
            'document_id'  => $this->formatValue($toMap->DocumentId ?? $toMap->documentId ?? $toMap->document_id ?? null),
            'type'         => $this->formatValue($toMap->type ?? $toMap->Type ?? null),
            'associate_id' => $this->formatValue($toMap->AssociateId ?? $toMap->associateId ?? $toMap->associate_id ?? null),
            'contact_id'   => $this->formatValue($toMap->ContactId ?? $toMap->contactId ?? $toMap->contact_id ?? null),
            'person_id'    => $this->formatValue($toMap->PersonId ?? $toMap->personId ?? $toMap->person_id ?? null),
            'project_id'   => $this->formatValue($toMap->ProjectId ?? $toMap->projectId ?? $toMap->project_id ?? null),
            'our_ref'      => $this->formatValue($toMap->OurRef ?? $toMap->ourRef ?? $toMap->ourref ?? null),
            'your_ref'     => $this->formatValue($toMap->YourRef ?? $toMap->yourRef ?? $toMap->yourref ?? null),
            'attention'    => $this->formatValue($toMap->Attention ?? $toMap->attention ?? null),
            'subject'      => $this->formatValue($toMap->Subject ?? $toMap->subject ?? null),
            'created_at'   => $this->formatValue($toMap->RegisteredDate ?? $toMap->registeredDate ?? $toMap->registered_date ?? null),
        ];
    }
}
