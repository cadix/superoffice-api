<?php

namespace Cadix\SuperOfficeApi\Maps;

/**
 * TODO
 */
class SaleMap extends Mapper
{
    public function map(object $toMap): object
    {
        return (object)[
            'sale_id'         => $this->formatValue($toMap->id ?? $toMap->saleid ?? $toMap->saled ?? $toMap->SaleId ?? null),
            'name'            => $this->formatValue($toMap->name ?? $toMap->Name ?? $toMap->nameDepartment ?? null),
            'relation_number' => $this->formatValue($toMap->number ?? $toMap->Number ?? $toMap->Number2 ?? $toMap->number2 ?? null),
            'department'      => $this->formatValue($toMap->department ?? $toMap->Department ?? null),
            'orgnr'           => $this->formatValue($toMap->orgnr ?? $toMap->OrgNr ?? null),
            'country'         => $this->formatValue($toMap->Country[ 'Name' ] ?? $toMap->Country ?? $toMap->country ?? null),
            'domain_name'     => $this->formatValue($this->countryCode($toMap->Country[ 'Name' ] ?? $toMap->Country ?? $toMap->country ?? null)),
            'business_id'     => $this->formatValue($toMap->Business->Id ?? $toMap->Business[ 'Id' ] ?? null),
            'category_id'     => $this->formatValue($toMap->Category->Id ?? $toMap->Category[ 'Id' ] ?? null),
            'phone'           => $this->formatValue($toMap->Phones[ 0 ]['StrippedValue'] ?? $toMap->{'salePhone/formattedNumber'} ?? null),
            'associate'       => [
                'associate_id' => $this->formatValue($toMap->Associate[ 'associate_id' ] ?? $toMap->Associate[ 'AssociateId' ] ?? $toMap->Associate[ 'associateId' ] ?? null),
                'person_id'    => $this->formatValue($toMap->{'saleAssociate/personId'} ?? $toMap->associate->PersonId ?? $toMap->Associate[ 'PersonId' ] ?? null),
                'first_name'   => $this->formatValue($toMap->{'saleAssociate/firstName'} ?? null),
                'last_name'    => $this->formatValue($toMap->{'saleAssociate/lastName'} ?? null),
                'insertion'    => $this->formatValue($toMap->{'saleAssociate/middleName'} ?? null),
                'email'        => $this->formatValue($toMap->{'saleAssociate/personEmail'} ?? null),
                'ej_user_id'   => $this->formatValue($toMap->Associate->EjUserId ?? null),
            ],
            'post_address'    => [
                'id'      => $this->formatValue($toMap->{'postAddress/addressId'} ?? null),
                'street'  => $this->formatValue($toMap->{'postAddress/line1'} ?? $toMap->Address[ 'Postal' ][ 'Address1' ] ?? null),
                'city'    => $this->formatValue($toMap->{'postAddress/city'} ?? $toMap->Address[ 'Postal' ][ 'City' ] ?? null),
                'zipcode' => $this->formatValue($toMap->{'postAddress/zip'} ?? $toMap->Address[ 'Postal' ][ 'Zipcode' ] ?? null),
            ],
            'street_address'  => [
                'id'      => $this->formatValue($toMap->{'streetAddress/addressId'} ?? null),
                'street'  => $this->formatValue($toMap->{'streetAddress/line1'} ?? $toMap->Address[ 'Postal' ][ 'Address1' ] ?? null),
                'city'    => $this->formatValue($toMap->{'streetAddress/city'} ?? $toMap->Address[ 'Postal' ][ 'City' ] ?? null),
                'zipcode' => $this->formatValue($toMap->{'streetAddress/zip'} ?? $toMap->Address[ 'Postal' ][ 'Zipcode' ] ?? null),
            ],
        ];
    }
}
