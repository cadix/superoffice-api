<?php

namespace Cadix\SuperOfficeApi\Maps;

class PersonMap extends Mapper
{
    public function map(object $toMap): object
    {
        return (object) [
            'person_id'     => $this->formatValue($toMap->id ?? $toMap->person_id ?? $toMap->personId ?? $toMap->PersonId ?? null),
            'first_name'    => $this->formatValue($toMap->firstName ?? $toMap->firstname ?? $toMap->FirstName ?? $toMap->Firstname ?? null),
            'last_name'     => $this->formatValue($toMap->lastName ?? $toMap->lastname ?? $toMap->LastName ?? $toMap->Lastname ?? null),
            'insertion'     => $this->formatValue($toMap->middleName ?? $toMap->middlename ?? $toMap->MiddleName ?? $toMap->Middlename ?? null),
            'full_name'     => $this->formatValue($toMap->full_name ?? $toMap->fullName ?? $toMap->FullName ?? null),
            'contact_id'    => $this->formatValue($toMap->contact_id ?? $toMap->contactId ?? $toMap->Contact->ContactId ?? $toMap->Contact['ContactId'] ?? null),
            'salutation'    => $this->formatValue($toMap->mrmrs ?? $toMap->mrMrs ?? $toMap->Mrmrs ?? null),
            'country'       => $this->formatValue($toMap->person_country ?? $toMap->personCountry ?? $toMap->Country->Name ?? $toMap->Country['Name'] ?? null),
            'domain_name'   => $this->formatValue($this->countryCode($toMap->person_country ?? $toMap->personCountry ?? $toMap->Country->Name ?? $toMap->Country['Name'] ?? null)),
            'email'         => $this->formatValue($toMap->{'email/emailAddress'} ?? $toMap->Emails[0]->Value ?? $toMap->Emails[0]['Value'] ?? null),
            'person_number' => $this->formatValue($toMap->person_number ?? $toMap->personNumber ?? $toMap->PersonNumber ?? null),
            'mobile'        => $this->formatValue($toMap->{'personMobilePhone/formattedNumber'} ?? $toMap->MobilePhones[0]->Value ?? $toMap->MobilePhones[0]['Value'] ?? null),
            'phone'         => $this->formatValue($toMap->{'personDirectPhone/formattedNumber'} ?? null),
            'business_id'   => $this->formatValue($toMap->Business->Id ?? $toMap->Business['Id'] ?? null),
            'category_id'   => $this->formatValue($toMap->Category->Id ?? $toMap->Category['Id'] ?? null),
            'address'       => [
                'street'  => $this->formatValue($toMap->{'personAddress/line1'} ?? $toMap->Address->Street->Address1 ?? $toMap->Address['Street']['Address1'] ?? null),
                'city'    => $this->formatValue($toMap->{'personAddress/city'} ?? $toMap->Address->Street->City ?? $toMap->Address['Street']['City'] ?? null),
                'zipcode' => $this->formatValue($toMap->{'personAddress/zip'} ?? $toMap->Address->Street->Zipcode ?? $toMap->Address['Street']['Zipcode'] ?? null),
            ],
            'associate' => [
                'person_id'    => $this->formatValue($toMap->{'personAssociate/personId'} ?? $toMap->Associate->PersonId ?? $toMap->Associate['PersonId'] ?? null),
                'email'        => $this->formatValue($toMap->{'personAssociate/personEmail'} ?? $toMap->Associate->PersonEmail ?? $toMap->Associate['PersonEmail'] ?? null),
                'associate_id' => $this->formatValue($toMap->{'personAssociate/associateId'} ?? $toMap->Associate->AssociateId ?? $toMap->Associate['AssociateId'] ?? null),
            ],
        ];
    }
}
