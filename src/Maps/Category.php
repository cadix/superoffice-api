<?php

namespace Cadix\SuperOfficeApi\Maps;

class Category
{
    public int         $id;
    public string|null $name;
    public string|null $tooltip;
    public int|null    $rank;
    public bool|null   $deleted;

    public function __construct(array|null $array = null)
    {
        if ($array) {
            $this->fillFromArray((object)$array);
        }
    }

    protected function fillFromArray(object $object): void
    {
        $this->id = $object->id ?? $object->Id ?? $object->CategoryId ?? null;
        $this->name = $object->Name ?? null;
        $this->tooltip = (! empty($object->Tooltip) ? $object->Tooltip : null) ?? null;
        $this->rank = $object->Rank ?? null;
        $this->deleted = $object->Deleted ?? null;
    }
}
