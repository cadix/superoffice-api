<?php

namespace Cadix\SuperOfficeApi\Maps;

class UserMap extends Mapper
{
    public function map(object $toMap): object
    {
        return (object) [
            'user_id'      => $this->formatValue($toMap->user_id ?? $toMap->userId ?? $toMap->PrimaryKey ?? null),
            'full_name'    => $this->formatValue($toMap->name ?? $toMap->person->FullName ?? $toMap->fullName ?? $toMap->FullName ?? null),
            'first_name'   => $this->formatValue($toMap->person->Firstname ?? $toMap->firstName ?? null),
            'last_name'    => $this->formatValue($toMap->person->Lastname ?? $toMap->lastName ?? null),
            'insertion'    => $this->formatValue($toMap->person->MiddleName ?? $toMap->middleName ?? $toMap->MiddelName ?? null),
            'email'        => $this->formatValue($toMap->person->Email ?? $toMap->personEmail ?? null),
            'person_id'    => $this->formatValue($toMap->person_id ?? $toMap->Personid ?? $toMap->PersonId ?? $toMap->personId ?? null),
            'ej_user_id'   => $this->formatValue($toMap->ejUserId ?? null),
            'associate_id' => $this->formatValue($toMap->associateDbId ?? null),
        ];
    }
}
