<?php

namespace Cadix\SuperOfficeApi;

use Illuminate\Http\RedirectResponse;

class Auth extends Model
{
    protected ?string $model = 'User';

    public function login(): RedirectResponse
    {
        if (! empty($this->client->getAccessToken())) {
            return back();
        }

        $url = sprintf(
            'https://%s.superoffice.com/login/common/oauth/authorize?client_id=%s&scope=openid&redirect_uri=%s&response_type=code',
            config('superoffice.environment'),
            config('superoffice.client_id'),
            config('superoffice.redirect_url')
        );

        return redirect()->away($url);
    }

    public function requestAccessToken(string $code): object
    {
        $this->client->url = sprintf(
            'https://%s.superoffice.com/login/common/oauth/tokens?client_id=%s&client_secret=%s&code=%s&redirect_uri=%s&grant_type=authorization_code',
            config('superoffice.environment'),
            config('superoffice.client_id'),
            config('superoffice.client_secret'),
            $code,
            config('superoffice.redirect_url')
        );

        return (object) $this->client->post();
    }

    public function check(): bool
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/currentPrincipal';

        try {
            return (bool)$this->get();
        } catch (\Exception $exception) {
            return false;
        }
    }
}
