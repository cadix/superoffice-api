<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Facades\Auth;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Validators\JWTValidator;
use Exception;
use Illuminate\Http\RedirectResponse;

class Session
{
    private ?string $access_token = null;

    private ?int $expires = null;

    private ?string $refresh_token = null;

    public function __construct()
    {
        if (auth()->check() && auth()->user()->superofficeOauth) {
            $this->setAccessToken(auth()->user()->superofficeOauth->access_token);
            $this->setRefreshToken(auth()->user()->superofficeOauth->refresh_token);
        }
    }

    public function setAccessToken(string $token): void
    {
        $this->access_token = $token;
    }

    public function updateAccessToken(string $refresh_token, string $access_token): bool
    {
        $this->access_token = $access_token;

        return SuperOfficeOAuth::where('refresh_token', $refresh_token)
            ->update(['access_token' => $access_token]);
    }

    public function getAccessToken(): ?string
    {
        return $this->access_token;
    }

    public function setRefreshToken(string $refresh_token): void
    {
        $this->refresh_token = $refresh_token;
    }

    /**
     * If there is no refresh token changes are that the user isn't logged in.
     *
     * @return string|RedirectResponse
     */
    public function getRefreshToken(): string | RedirectResponse
    {
        return $this->refresh_token ?? Auth::login();
    }

    /**
     * @param mixed $model
     *
     * @throws \Exception
     */
    public function setTokens($model, string $access_token, string $refresh_token, ?int $user = null): SuperOfficeOAuth
    {
        if (! in_array(\Cadix\SuperOfficeApi\Traits\HasSuperOfficeOAuth::class, class_uses_recursive($model))) {
            throw new Exception('Required HasSuperOfficeOAuth trait not used');
        }

        return SuperOfficeOAuth::updateOrCreate(
            [
                'user_id'   => $model->id,
                'user_type' => get_class($model),
            ],
            [
                'superoffice_user_id' => $user,
                'access_token'        => $access_token,
                'refresh_token'       => $refresh_token,
            ]
        );
    }

    public function refreshAccessToken(): bool
    {
        $url = sprintf(
            'https://%s.superoffice.com/login/common/oauth/tokens?%s',
            config('superoffice.environment'),
            $this->getRefreshTokenParams()
        );
        $content = (object) $this->request('post', $url);

        // Validate the provided JWT
        (new JWTValidator())->validateAndVerifyJwt($content->id_token);

        return $this->updateAccessToken($this->refresh_token, $content->access_token);
    }

    public function getRefreshTokenParams(): string
    {
        $params = [
            'grant_type'    => 'refresh_token',
            'client_id'     => config('superoffice.client_id'),
            'client_secret' => config('superoffice.client_secret'),
            'refresh_token' => $this->refresh_token,
            'redirect_uri'  => config('superoffice.redirect_url'),
            'scope'         => 'openid',
        ];

        return http_build_query($params);
    }
}
