<?php

namespace Cadix\SuperOfficeApi\Validators;

use Cadix\SuperOfficeApi\JWT;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Token\Plain;
use Lcobucci\JWT\Validation\RequiredConstraintsViolated;

class JWTValidator extends JWT
{
    /**
     * Validates and verifies a given JWT token.
     *
     * @param string $jwt the raw, encoded JWT token
     *
     * @return bool returns TRUE if the token could be verified and validated
     *
     * @throws \InvalidArgumentException throws if the provided token is invalid
     */
    public function validateAndVerifyJwt(string $jwt): bool
    {
        assert($this->configuration instanceof Configuration);
        $token = $this->configuration->parser()->parse($jwt);
        assert($token instanceof Plain);

        $constraints = $this->configuration->validationConstraints();

        try {
            $this->configuration->validator()->assert($token, ...$constraints);
        } catch (RequiredConstraintsViolated $exception) {
            throw $exception;
        }

        return true;
    }
}
