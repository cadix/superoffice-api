<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\Category as CategoryMap;

class Category extends Model
{
    protected string $model = 'List/Category/Items';

    /**
     * @return array<CategoryMap>|null
     */
    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::get();

        $categories = null;
        foreach ($response as $category) {
            $categories[] = new CategoryMap($category);
        }

        return $categories;
    }

    /**
     * @return array<CategoryMap>|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::get();

        $categories = null;
        foreach ($response as $category) {
            $categories[] = new CategoryMap($category);
        }

        return $categories;
    }

    public function find(int $id): CategoryMap|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        return new CategoryMap(parent::get());
    }
}
