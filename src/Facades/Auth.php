<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Auth as RootAuth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootAuth
 *
 * @method static RedirectResponse login()
 * @method static object requestAccessToken(string $code)
 * @method static bool check()
 */
class Auth extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootAuth::class;
    }
}
