<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Contact as RootContact;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootContact
 *
 * @method static object|null find(int $id)
 * @method static array|null all()
 * @method static array|null get()
 * @method static object create(array $attributes)
 * @method static object default()
 * @method static object update(int $id, array $attributes )
 * @method static bool validate(array $contact)
 * @method static bool delete(int $id)
 */
class Contact extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootContact::class;
    }
}
