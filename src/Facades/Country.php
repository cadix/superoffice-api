<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Country as RootCountry;
use Cadix\SuperOfficeApi\Maps\Country as CountryMap;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootCountry
 *
 * @method static array|null all()
 * @method static array|null get()
 * @method static CountryMap|null find(int $id)
 */
class Country extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootCountry::class;
    }
}
