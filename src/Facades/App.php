<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\App as RootApp;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootApp
 *
 * @method static array status()
 */
class App extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootApp::class;
    }
}
