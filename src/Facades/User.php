<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\User as RootUser;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootUser
 *
 * @method static array|null all()
 * @method static array|null get()
 * @method static object|null itsAMeMario()
 */
class User extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootUser::class;
    }
}
