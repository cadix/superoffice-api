<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Category as RootCategory;
use Cadix\SuperOfficeApi\Maps\Category as CategoryMap;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootCategory
 *
 * @method static array|null all()
 * @method static array|null get()
 * @method static CategoryMap|null find(int $id)
 */
class Category extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootCategory::class;
    }
}
