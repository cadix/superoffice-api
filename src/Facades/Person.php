<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Person as RootPerson;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootPerson
 *
 * @method static object|null find(int $id)
 * @method static array|null all()
 * @method static array|null get()
 * @method static object create(array $attributes)
 * @method static object default()
 * @method static object update(int $id, array $attributes )
 * @method static bool validate(array $person)
 * @method static bool delete(int $id)
 */
class Person extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootPerson::class;
    }
}
