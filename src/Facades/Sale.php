<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Sale as RootSale;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootSale
 *
 * @method static object|null find(int $id)
 * @method static array|null all()
 * @method static array|null get()
 * @method static object create(array $attributes)
 * @method static object default()
 * @method static object update(int $id, array $attributes )
 * @method static bool validate(array $contact)
 * @method static bool delete(int $id)
 */
class Sale extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootSale::class;
    }
}
