<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Currency as RootCurrency;
use Cadix\SuperOfficeApi\Maps\Currency as CurrencyMap;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootCurrency
 *
 * @method static array|null all()
 * @method static array|null get()
 * @method static CurrencyMap|null find(int $id)
 */
class Currency extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootCurrency::class;
    }
}
