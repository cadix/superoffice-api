<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Mdo as RootMdo;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootMdo
 *
 * @method static array|null getList(string $listName)
 */
class Mdo extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootMdo::class;
    }
}
