<?php

namespace Cadix\SuperOfficeApi\Facades;

use Cadix\SuperOfficeApi\Business as RootBusiness;
use Cadix\SuperOfficeApi\Maps\Business as BusinessMap;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootBusiness
 *
 * @method static array|null all()
 * @method static array|null get()
 * @method static BusinessMap|null find(int $id)
 */
class Business extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootBusiness::class;
    }
}
