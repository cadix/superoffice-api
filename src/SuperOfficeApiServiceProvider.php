<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Console\InstallSuperOfficeApiPackage;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class SuperOfficeApiServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/superoffice.php', 'superoffice');

        $this->app->singleton(Client::class, function () {
            return new Client();
        });

        $this->app->singleton(App::class, function () {
            $client = app(Client::class);

            return new App($client);
        });

        $this->app->singleton(Auth::class, function () {
            $client = app(Client::class);

            return new Auth($client);
        });

        $this->app->singleton(Business::class, function () {
            $client = app(Client::class);

            return new Business($client);
        });

        $this->app->singleton(Category::class, function () {
            $client = app(Client::class);

            return new Category($client);
        });

        $this->app->singleton(Contact::class, function () {
            $client = app(Client::class);

            return new Contact($client);
        });

        $this->app->singleton(Country::class, function () {
            $client = app(Client::class);

            return new Country($client);
        });

        $this->app->singleton(Document::class, function () {
            $client = app(Client::class);

            return new Document($client);
        });

        $this->app->singleton(Mdo::class, function () {
            $client = app(Client::class);

            return new Mdo($client);
        });


        $this->app->singleton(Person::class, function () {
            $client = app(Client::class);

            return new Person($client);
        });

        $this->app->singleton(User::class, function () {
            $client = app(Client::class);

            return new User($client);
        });
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallSuperOfficeApiPackage::class,
            ]);

            $this->publishes([
                __DIR__ . '/../config/superoffice.php' => config_path('superoffice.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../database/migrations/create_superoffice_oauth_table.php' => database_path('migrations/' . Carbon::now()->format('Y_m_d_His') . '_create_superoffice_oauth_table.php'),
            ], 'migrations');
        }
    }
}
