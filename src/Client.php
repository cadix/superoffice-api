<?php

namespace Cadix\SuperOfficeApi;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Http;

class Client extends Session
{
    public string|null $url = null;
    /**
     * @var string[]|null
     */
    public array|null $params = null;

    /**
     * @var string[]|null
     */
    public array|null $headers = null;
    /**
     * @var string[]|null
     */
    private array|null $previousRequest = null;

    public function get(): array|null
    {
        return $this->request('GET', $this->url, $this->headers);
    }

    public function post(array|null $body = null): array|null
    {
        if (! $body) {
            $body = [];
        }

        return $this->request('POST', $this->url, $this->headers, $body);
    }

    public function put(array $body): array|null
    {
        return $this->request('PUT', $this->url, $this->headers, $body);
    }

    public function delete(): array|null
    {
        return $this->request('DELETE', $this->url, $this->headers);
    }

    /**
     * @param array<string>|null $headers
     * @param array<string>      $body
     *
     * @return array|null [string, int, array, object]|null;
     *
     * @throws GuzzleException
     * @throws Exception
     */
    protected function request(string $method, string $url, array|null $headers = null, array $body = [], int $tries = 0): array|null
    {
        $response = Http::timeout(config('superoffice.timeout'))
            ->withOptions([
                'debug' => config('app.deug'),
            ]);

        if (is_array($headers) && count($headers) > 0) {
            $response = $response->withHeaders($headers);
        }

        if ($method === 'GET') {
            $response = $response->get($url, $this->params);
        }

        if ($method !== 'GET') {
            if ($this->params) {
                $url .= urldecode(http_build_query($this->params));
            }

            $response = $response->$method($url, $body);
        }

        if ($response->failed() && $this->getRefreshToken()) {
            if ($tries >= config('superoffice.retries')) {
                $message = 'More than '.$tries.' tries for request '.$url.' without success with status '.$response->status().'.';
                if (is_array($response->json()) && ! empty($response->json()['Message'])) {
                    $message .= ' Possible reason: '. $response->json()['Message'];
                }

                throw new Exception($message);
            }
            $this->refreshAccessToken();

            return $this->request($method, $url, $headers, $body, ++$tries);
        }

        return $response->json();
    }
}
