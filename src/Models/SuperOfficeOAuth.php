<?php

namespace Cadix\SuperOfficeApi\Models;

use Cadix\SuperOfficeApi\Database\Factories\SuperOfficeOAuthFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property string   $access_token
 * @property string   $refresh_token
 * @property int|null $superoffice_user_id
 * @property int      $user_id
 * @property string   $user_type
 */
class SuperOfficeOAuth extends Model
{
    use HasFactory;

    protected $table = 'superoffice_oauth';

    protected $fillable = [
        'access_token',
        'refresh_token',
        'superoffice_user_id',
        'user_id',
        'user_type',
    ];

    protected $casts = [
        'id'                   => 'integer',
        'super_office_user_id' => 'integer',
        'user_id'              => 'integer',
    ];

    public function user(): MorphTo
    {
        return $this->morphTo();
    }

    protected static function newFactory(): SuperOfficeOAuthFactory
    {
        return SuperOfficeOAuthFactory::new();
    }
}
