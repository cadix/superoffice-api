<?php

namespace Cadix\SuperOfficeApi;

class Model
{
    public Client $client;
    private string $baseUrl;

    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->baseUrl = sprintf(
            'https://%s.superoffice.com/%s/api/v1/',
            config('superoffice.environment'),
            config('superoffice.customer_id')
        );
    }

    public function all(): array|null
    {
        $this->setHeaders();

        return $this->client->get();
    }

    public function paginate(int $perPage = 10, int $page = 1): object
    {
        $model = $this->getModel();
        $currentParams = $this->client->params;

        // Get the total items
        $modelClass = '\\Cadix\\SuperOfficeApi\\Facades\\'.$model;
        $totalItems = $modelClass::select([$model.'Id', 'getAllRows'])->get();

        $this->client->params = $currentParams;
        $result = $this->limit($perPage)
            ->skip(($page - 1) * $perPage)
            ->get();

        return (object) [
            'total'        => count($totalItems),
            'per_page'     => $perPage,
            'current_page' => $page,
            'last_page'    => (int) ceil(count($totalItems) / $perPage),
            'from'         => 1 === $page ? 1 : $page * $perPage,
            'to'           => 1 === $page ? $perPage : ($page * $perPage) + $perPage,
            'data'         => (array) $result,
        ];
    }

    public function destroy(int $id): bool
    {
        $this->setHeaders();

        return is_null($this->client->delete());
    }

    /**
     * Comma separated list of column names to return.
     *
     * @param string|array $columns
     */
    public function select(array | string $columns): self
    {
        $this->client->params['$select'] = $this->stringifyArray($columns);

        return $this;
    }

    /**
     * Expression to restrict the results. e.g.: "name begins 'foo' and category gt 1".
     */
    public function filter(string $filter): self
    {
        $this->client->params['$filter'] = $this->stringifyArray($filter);

        return $this;
    }

    /**
     * Comma separated list of column names to sort by, with optional direction. "name asc,fullname,category desc".
     *
     * @param array|string $orderBy
     */
    public function orderBy(array | string $orderBy): self
    {
        if (is_array($orderBy)) {
            $list = '';
            $i = 0;
            foreach ($orderBy as $by => $order) {
                if ($i > 0) {
                    $list .= ',';
                }

                $list .= sprintf(
                    '%s %s',
                    $by,
                    $order
                );
                ++$i;
            }

            $orderBy = $list;
        }

        $this->client->params['$orderBy'] = $this->stringifyArray($orderBy);

        return $this;
    }

    /**
     * Comma separated list of entity names to use. Default = "contact". "contact, person".
     *
     * @param array|string $entities
     */
    public function entities(array | string $entities): self
    {
        $this->client->params['$entities'] = $this->stringifyArray($entities);

        return $this;
    }

    /**
     * Number of rows to return in results.
     */
    public function top(int $limit): self
    {
        $this->client->params['$top'] = $limit;

        return $this;
    }

    /**
     * Number of rows to return in results.
     */
    public function limit(int $limit): self
    {
        return $this->top($limit);
    }

    /**
     * Number of rows from database to skip before returning results.
     */
    public function skip(int $skip): self
    {
        $this->client->params['$skip'] = $skip;

        return $this;
    }

    /**
     * FULL (with raw values and hints for each value) or SLIM (just the display values) or WIDE.
     *
     * @param $mode
     */
    public function mode($mode): self
    {
        $this->client->params['$mode'] = $mode;

        return $this;
    }

    /**
     * Provider specific options.
     * e.g: "GrandTotal=true".
     *
     * @param array|string $options
     */
    public function options(array | string $options): self
    {
        $this->client->params['$options'] = $this->stringifyArray($options);

        return $this;
    }

    /**
     * Provider specific context parameter.
     */
    public function context(string $context): self
    {
        $this->client->params['$context'] = $context;

        return $this;
    }

    /**
     * Set XML or JSON output format; override the format determined from Accept header.
     */
    public function format(string $format): self
    {
        $this->client->params['$format'] = $format;

        return $this;
    }

    /**
     * Make output names into JSON safe property names. Replace all unsafe characters with _ underscore.
     */
    public function jsonSafe(bool $jsonSafe): self
    {
        $this->client->params['$jsonSafe'] = $jsonSafe;

        return $this;
    }

    /**
     * Return Logical or Display values in SLIM mode. Logical returns true/false for booleans, Display returns icon hints. Dates are always returned as ISO strings.
     */
    public function output(string $output): self
    {
        $this->client->params['$output'] = $output;

        return $this;
    }

    private function stringifyArray(array | string | int $array): string
    {
        if (is_array($array)) {
            return implode(',', $array);
        }

        return $array;
    }

    public function get(): array | null
    {
        $this->setHeaders();

        return $this->client->get();
    }

    public function post(?array $body = null): array | null
    {
        $this->setHeaders();

        return $this->client->post($body);
    }

    public function put($body): array | null
    {
        $this->setHeaders();

        return $this->client->put($body);
    }

    private function setHeaders(): void
    {
        $this->client->headers = [
            'Authorization'   => 'Bearer '.$this->client->getAccessToken(),
            'Accept'          => 'application/json; charset=utf-8',
            'Accept-Language' => 'en',
            'Content-Type'    => 'application/json; charset=utf-8',
        ];
    }

    public function getModel(): string
    {
        return $this->model ?? class_basename($this);
    }

    public function getUrl(): string
    {
        return $this->client->url;
    }

    public function getParams(): array|null
    {
        return $this->client->params;
    }

    public function getHeaders(): array|null
    {
        $this->setHeaders();

        return $this->client->headers;
    }

    protected function getBaseUrl(): string
    {
        return $this->baseUrl;
    }
}
