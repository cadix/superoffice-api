<?php

namespace Cadix\SuperOfficeApi;

use Illuminate\Support\Facades\Cache;
use Spatie\GuzzleRateLimiterMiddleware\Store;

class RateLimiterStore implements Store
{
    public function get(): array
    {
        return Cache::get('superoffice-rate-limiter', []);
    }

    public function push(int $timestamp, int $limit)
    {
        Cache::put('superoffice-rate-limiter', array_merge($this->get(), [$timestamp]));
    }
}
