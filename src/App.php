<?php

namespace Cadix\SuperOfficeApi;

class App
{
    public function __construct(public Client $client)
    {
    }

    /**
     * @return array<string>
     */
    public function status(): array
    {
        $this->client->url = sprintf(
            'https://%s.superoffice.com/api/state/%s',
            config('superoffice.environment'),
            config('superoffice.customer_id')
        );

        return $this->client->get();
    }
}
