<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\UserMap;

class User extends Model
{
    protected string $model = 'User';

    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();
        $map = new UserMap();

        return (array) $map($response);
    }

    /**
     * @return array|null
     *
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;
        $response = parent::get();

        $map = new UserMap();

        return ! $response ? $response : (array) $map($response['value']);
    }

    public function itsAMeMario(): object|null
    {
        $this->client->url .= parent::getBaseUrl().$this->model.'/currentPrincipal';

        $response = parent::get();

        $map = new UserMap();

        return (object) $map((array) $response);
    }
}
