<?php

namespace Cadix\SuperOfficeApi\Traits;

use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasSuperOfficeOAuth
{
    public function superofficeOauth(): MorphOne
    {
        return $this->morphOne(SuperOfficeOAuth::class, 'user');
    }

    public function scopeHasSuperOffice(Builder $query): Builder
    {
        $model = get_class($this);
        $table = app($model)->getTable();
        $primaryKey = app($model)->getKeyName();
        $driver = app($model)->getConnection()->getDriverName();

        switch ($driver) {
            case 'mysql':
                return $query->selectRaw("IF((SELECT `superoffice_oauth`.`id` FROM `superoffice_oauth` WHERE `superoffice_oauth`.`user_id` = `{$table}`.`{$primaryKey}` AND `superoffice_oauth`.`user_type` = ? LIMIT 1) IS NULL, 0, 1) AS `superoffice`", [$model]);

            default:
                return $query->selectSub('SELECT '.(bool) $this->superofficeOauth, 'superoffice');
        }
    }
}
