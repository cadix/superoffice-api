<?php

namespace Cadix\SuperOfficeApi;

use Cadix\SuperOfficeApi\Maps\DocumentMap;

class Document extends Model
{
    protected string $model = 'Document';

    /**
     * Gets a DocumentEntity object.
     *
     * @param  int         $id
     * @return object|null
     */
    public function find(int $id): object|null
    {
        $this->client->url = parent::getBaseUrl().$this->model.'/'.$id;

        $map = new DocumentMap();
        $response = parent::get();

        return (object) $map((array) $response);
    }

    public function all(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;

        $response = parent::all();
        $map = new DocumentMap();

        return (array) $map($response);
    }

    /**
     * @return array|null
     */
    public function get(): array|null
    {
        $this->client->url = parent::getBaseUrl().$this->model;
        $response = parent::get();

        $map = new DocumentMap();

        return (array) $map($response['value']);
    }
}
