<?php

namespace Cadix\SuperOfficeApi;

use DateTimeImmutable;
use Lcobucci\Clock\FrozenClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\ValidAt;

class JWT
{
    public Configuration $configuration;

    public function __construct()
    {
        $now = new FrozenClock(new DateTimeImmutable());
        $key = InMemory::file(config('superoffice.private_key'));

        $this->configuration = Configuration::forSymmetricSigner(new Sha256(), $key);
        $this->configuration->setValidationConstraints(
            new IssuedBy(sprintf('https://%s.superoffice.com', config('superoffice.environment'))),
            new ValidAt($now, new \DateInterval('PT30S'))
        );
    }

    /**
     * Method used for testing.
     */
    public function generateJWT(): string
    {
        $now = new DateTimeImmutable();

        return $this->configuration->builder()
            // Configures the issuer (iss claim)
            ->issuedBy(sprintf('https://%s.superoffice.com', config('superoffice.environment')))
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($now)
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($now->modify('+10 minutes'))
            // Configures a new claim, called "uid"
            ->withClaim('uid', 1)
            // Builds a new token
            ->getToken($this->configuration->signer(), $this->configuration->signingKey())
            ->toString();
    }
}
