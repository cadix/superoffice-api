<?php

namespace Cadix\SuperOfficeApi\Tests;

use Cadix\SuperOfficeApi\Database\Factories\UserFactory;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Traits\HasSuperOfficeOAuth;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

/**
 * @property string           $name
 * @property string           $email
 * @property string           $password
 * @property string           $rememberToken
 * @property SuperOfficeOAuth $superofficeOauth
 * @property bool             $superoffice
 */
class User extends Model implements AuthorizableContract, AuthenticatableContract
{
    use HasSuperOfficeOAuth;
    use Authorizable;
    use Authenticatable;
    use HasFactory;

    protected $guarded = [];

    protected $table = 'users';

    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }
}
