<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Person;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PersonTest extends TestCase
{
    private string $model = 'Person';

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_get_all(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__ . '/../_sample-responses/Person/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertIsArray(Person::all());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_use_get(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__ . '/../_sample-responses/Person/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $this->assertIsArray(Person::get());

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_find_by_id(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__ . '/../_sample-responses/Person/find.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 9;
        $person = Person::find($id);

        $this->assertIsObject($person);
        $this->assertEquals($id, $person->person_id);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model . '/' . $id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function it_can_create_a_person(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $person = Person::create([
            'type'            => 'bedrijf',
            'salutation'      => 'Mr',
            'first_name'      => 'Hendrik',
            'insertion'       => 'van',
            'last_name'       => 'Dijk',
            'relation_number' => 243534,
            'mobile'          => '1234567890',
            'domain_name'     => 'NL',
            'email'           => 'h.vdijk@cadix.nl',
            'contact_id'      => 2,
            'street'          => 'Galjoot 1',
            'postcode'        => '8997DF',
            'city'            => 'Groningen',
            'associate_id'    => null,
        ]);

        $this->assertTrue(is_object($person));

        Http::assertSent(function (Request $request) use ($oauth) {
            return (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ) . $this->model . '/Validate' &&
                    $request->hasHeaders([
                        'Bearer',
                        'Accept',
                        'Content-Type',
                    ]) &&
                    $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ) . $this->model &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'POST'
                );
        });
    }

    /**
     * @test
     */
    public function it_can_update_a_person(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 1;
        $person = Person::update($id, [
            'person_id'       => $id,
            'type'            => 'bedrijf',
            'salutation'      => 'Mr',
            'first_name'      => 'Hendrik',
            'insertion'       => 'van',
            'last_name'       => 'Dijk',
            'relation_number' => 243534,
            'mobile'          => '1234567890',
            'domain_name'     => 'NL',
            'email'           => 'h.vdijk@cadix.nl',
            'contact_id'      => 2,
            'street'          => 'Galjoot 1',
            'postcode'        => '8997DF',
            'city'            => 'Groningen',
            'associate_id'    => 12,
        ]);

        $this->assertTrue(is_object($person));

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/Validate' &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/'.$id &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'PUT'
                );
        });
    }

    /**
     * @test
     */
    public function it_can_delete_a_person(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 10;
        $deleted = Person::delete($id);

        $this->assertTrue($deleted);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model . '/' . $id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'DELETE';
        });
    }

    /**
     * @test
     */
    public function it_can_create_a_new_default_person(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(file_get_contents(__DIR__ . '/../_sample-responses/Person/default.json'), 200, ['Content-Type' => 'application/json']),
        ]);

        $default = Person::default();

        $this->assertIsObject($default);
        $this->assertObjectHasAttribute('person_id', $default);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . $this->model . '/default' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }
}
