<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Contact;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Rules\SuperOfficeExists;
use Cadix\SuperOfficeApi\Rules\SuperOfficeUnique;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class RulesTest extends TestCase
{
    /**
     * @test
     */
    public function return_true_if_model_exists(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(json_encode(['value' => [['id' => 1], ['id' => 2]]]), 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 3;
        $rule = new SuperOfficeExists(Contact::class, 'contactId');

        $this->assertTrue($rule->passes('contact_id', $id));

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . 'Contact?%24filter=contactId%20equals%20'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function return_false_if_model_does_not_exists(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(json_encode(['value' => []]), 200, ['Content-Type' => 'application/json']),
        ]);

        $rule = new SuperOfficeExists(Contact::class, 'contactId');

        $id = 3;
        $this->assertFalse($rule->passes('contact_id', $id));
        $this->assertSame($rule->message(), 'The selected contact_id is invalid.');

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . 'Contact?%24filter=contactId%20equals%20'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function return_true_if_model_is_unique(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(json_encode(['value' => []]), 200, ['Content-Type' => 'application/json']),
        ]);

        $rule = new SuperOfficeUnique(Contact::class, 'contactId', null);

        $id = 3;
        $this->assertTrue($rule->passes('contact_id', $id));

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . 'Contact?%24filter=contactId%20equals%20'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function return_false_if_model_is_not_unique(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $id = 3;
        Http::fake([
            '*' => Http::response(json_encode(['value' => [1, 2]]), 200, ['Content-Type' => 'application/json']),
        ]);

        $rule = new SuperOfficeUnique(Contact::class, 'contactId', null);

        $this->assertFalse($rule->passes('contact_id', $id));
        $this->assertSame($rule->message(), 'The contact_id has already been taken.');

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . 'Contact?%24filter=contactId%20equals%20'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function unique_will_ignore_id(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(json_encode(['value' => []]), 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 3;
        $except = 4;
        $rule = new SuperOfficeUnique(Contact::class, 'contactId', $except);

        $this->assertTrue($rule->passes('contact_id', $id));

        Http::assertSent(function (Request $request) use ($id, $except, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) . 'Contact?%24filter=contactId%20equals%20'.$id.'%20and%20contactId%20notOneOf%28'.$except.'%29' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }
}
