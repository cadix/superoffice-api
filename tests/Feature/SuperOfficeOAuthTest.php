<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Contact;
use Cadix\SuperOfficeApi\JWT;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Session;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Cadix\SuperOfficeApi\Tests\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class SuperOfficeOAuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_superoffice_oauth_has_required_fields(): void
    {
        $data = [
            'access_token'        => '34sdsd4dwe',
            'refresh_token'       => 'sdfsdf43rfsdsf43rfesfdsf34sdf',
            'superoffice_user_id' => 23,
            'user_id'             => 2,
            'user_type'           => 'App\Models\User',
        ];

        $oauth = SuperOfficeOAuth::factory()->create($data);

        $this->assertDatabaseHas('superoffice_oauth', $data);
    }

    /** @test */
    public function a_oauth_belongs_to_a_user(): void
    {
        // Given we have an author
        $user = User::factory()->create();
        // And this author has a Post

        $data = [
            'access_token'        => '34sdsd4dwe',
            'refresh_token'       => 'sdfsdf43rfsdsf43rfesfdsf34sdf',
            'superoffice_user_id' => 23,
        ];

        $user->superofficeOauth()->create($data);

        $data = array_merge(
            $data,
            [
                'user_id'   => $user->id,
                'user_type' => get_class($user),
            ]
        );

        $this->assertDatabaseHas('superoffice_oauth', $data);
    }

    /**
     * @test
     */
    public function user_has_superoffice_attribute(): void
    {
        $this->markTestIncomplete('Not possible without a mysql database');
        $oauth = SuperOfficeOAuth::factory()->create();
        /* TODO can't use mysql `if` with a nosql database
         $user = $oauth->user()->hasSuperOffice()->first();

        $this->assertTrue($user->superoffice);*/
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function it_refreshes_tokens_when_unauthorized_response_returned(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $refreshResponse = file_get_contents(__DIR__.'/../_sample-responses/login_tokens.json');
        $refreshResponse = json_decode($refreshResponse);
        $refreshResponse->id_token = ( new JWT() )->generateJWT();

        $responses = Http::fakeSequence()
            ->push('access_token', 401) // Expired access token
            ->push(json_encode($refreshResponse), 200)
            ->push(file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json'), 200, );

        $contacts = Contact::get();

        $this->assertDatabaseHas('superoffice_oauth', [
            'id'                  => (string) $oauth->id,
            'user_id'             => (string) $oauth->user_id,
            'access_token'        => $refreshResponse->access_token,
            'refresh_token'       => $oauth->refresh_token,
            'superoffice_user_id' => (string) $oauth->superoffice_user_id,
            'user_type'           => get_class(new User()),
        ]);
    }

    /**
     * @test
     */
    public function it_refreshes_tokens_when_server_error_response_returned(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $refreshResponse = file_get_contents(__DIR__.'/../_sample-responses/login_tokens.json');
        $refreshResponse = json_decode($refreshResponse);
        $refreshResponse->id_token = ( new JWT() )->generateJWT();

        Http::fake([
            '*' => Http::sequence()
                ->push('access_token', 500, )  // Expired access token
                ->push(json_encode($refreshResponse), 200)
                ->push(file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json'), 200),
        ]);

        $contacts = Contact::get();

        $this->assertDatabaseHas('superoffice_oauth', [
            'id'                  => (string) $oauth->id,
            'user_id'             => (string) $oauth->user_id,
            'access_token'        => $refreshResponse->access_token,
            'refresh_token'       => $oauth->refresh_token,
            'superoffice_user_id' => (string) $oauth->superoffice_user_id,
            'user_type'           => get_class(new User()),
        ]);
    }

    /**
     * @test
     */
    public function it_will_only_make_a_limited_amount_of_requests_on_failure(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        $refreshResponse = file_get_contents(__DIR__.'/../_sample-responses/login_tokens.json');
        $refreshResponse = json_decode($refreshResponse);
        $refreshResponse->id_token = ( new JWT() )->generateJWT();

        $responses = Http::fakeSequence()
            ->push('access_token', 401) // Expired access token
            ->push(json_encode($refreshResponse), 200)
            ->push('access_token', 401) // Expired access token
            ->push(json_encode($refreshResponse), 200)
            ->push('access_token', 401) // Expired access token
            ->push(json_encode($refreshResponse), 200)
            ->push(file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json'), 200, );

        $this->expectException(\Exception::class);
        $contacts = Contact::get();
    }

    /**
     * @test
     */
    public function can_not_send_more_than10_requests_per_second(): void
    {
        $this->markTestIncomplete('unable to test the rate limiter');
        /*$requestsAmount = 12;
        $requestsLimit = 10;

        $this->createRateLimiter($requestsLimit, RateLimiter::TIME_FRAME_SECOND);

        $content = file_get_contents(__DIR__.'/../_sample-responses/Contact/index.json');
        $mockResponses = [];
        for ($i = 0; $i < $requestsAmount; ++$i) {
            $mockResponses[] = Http::response($content, 200, ['Content-Type' => 'application/json']);
        }

        $responses = Http::fake();

        $stack = HandlerStack::create();
        $stack->push(RateLimiterMiddleWare::perSecond($requestsLimit, null, $this->deferrer));
        $this->assertSame(0, $this->deferrer->getCurrentTime());

        for ($j = 0; $j < $requestsAmount; ++$j) {
            $contacts = Contact::all();
        }

        $this->assertSame(1000, $this->deferrer->getCurrentTime());*/
    }

    /**
     * @test
     */
    public function a_oauth_model_must_have_required_trait(): void
    {
        $fakeUser = $this->getMockBuilder(\stdClass::class)->getMock();

        $this->expectException(\Exception::class);
        ( new Session() )->setTokens($fakeUser, 'dfg43rfsd', '3rff34r');
    }
}
