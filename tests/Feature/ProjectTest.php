<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\Project;
use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ProjectTest extends TestCase
{
    private string $model = 'Project';

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_get_all(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Project/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $projects = Project::all();

        $this->assertIsArray($projects);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_use_get(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Project/index.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $project = Project::get();

        $this->assertIsArray($project);

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function it_can_find_by_id(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);
        $content = file_get_contents(__DIR__.'/../_sample-responses/Project/find.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 78;
        $project = Project::find($id);

        $this->assertIsObject($project);
        $this->assertEquals($id, $project->project_id);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });
    }

    /**
     * @test
     */
    public function it_can_create_a_project(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $project = Project::create([
            'name'       => 'Vitens',
            'department' => 'Zwolle',
            'country'    => 'Netherlands',
            'person_id'  => 2,
            'address'    => [
                'postal' => [
                    'line1' => 'Stationstraat 2',
                    'zip'   => '3443AT',
                    'city'  => 'Zwolle',
                ],
            ],
            'phone' => 12345678990,
        ]);

        Http::assertSent(function (Request $request) use ($oauth) {
            return (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/Validate' &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model &&
                    $request->hasHeaders([
                        'Bearer',
                        'Accept',
                        'Content-Type',
                    ]) &&
                    $request->method() === 'POST'
                );
        });


        $this->assertTrue(is_object($project));
    }

    /**
     * @test
     */
    public function it_can_update_a_project(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response([], 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 1;
        $project = Project::update($id, [
            'project_id'   => 1,
            'name'         => 'Vitens',
            'department'   => 'Zwolle',
            'country'      => 'Netherlands',
            'associate_id' => 20,
            'business_id'  => 12,
            'category_id'  => 9,
            'address'      => [
                'postal' => [
                    'line1' => 'Stationstraat 2',
                    'zip'   => '3443AT',
                    'city'  => 'Zwolle',
                ],
            ],
        ]);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/Validate' &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'POST'
                )
                ||
                (
                    $request->url() === sprintf(
                        'https://%s.superoffice.com/%s/api/v1/',
                        config('superoffice.environment'),
                        config('superoffice.customer_id')
                    ).$this->model.'/'.$id &&
                    $request->hasHeaders([
                        'Authorization' => 'Bearer '.$oauth->access_token,
                        'Accept'        => 'application/json; charset=utf-8',
                        'Content-Type'  => 'application/json; charset=utf-8',
                    ]) &&
                    $request->method() === 'PUT'
                );
        });

        $this->assertIsObject($project);
    }

    /**
     * @test
     */
    public function it_can_delete_a_project(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(null, 200, ['Content-Type' => 'application/json']),
        ]);

        $id = 10;
        $deleted = Project::delete($id);

        Http::assertSent(function (Request $request) use ($id, $oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/'.$id &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'DELETE';
        });

        $this->assertTrue($deleted);
    }

    /**
     * @test
     */
    public function it_can_create_a_new_default_project(): void
    {
        $oauth = SuperOfficeOAuth::factory()->create();
        Auth::login($oauth->user);

        Http::fake([
            '*' => Http::response(file_get_contents(__DIR__.'/../_sample-responses/Project/default.json'), 200, ['Content-Type' => 'application/json']),
        ]);

        $default = Project::default();

        Http::assertSent(function (Request $request) use ($oauth) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/%s/api/v1/',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ).$this->model.'/default' &&
                $request->hasHeaders([
                    'Authorization' => 'Bearer '.$oauth->access_token,
                    'Accept'        => 'application/json; charset=utf-8',
                    'Content-Type'  => 'application/json; charset=utf-8',
                ]) &&
                $request->method() === 'GET';
        });

        $this->assertIsObject($default);
        $this->assertObjectHasAttribute('project_id', $default);
    }
}
