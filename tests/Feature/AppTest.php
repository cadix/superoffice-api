<?php

namespace Cadix\SuperOfficeApi\Tests\Feature;

use Cadix\SuperOfficeApi\Facades\App;
use Cadix\SuperOfficeApi\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class AppTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_get_the_app_status(): void
    {
        $content = file_get_contents(__DIR__.'/../_sample-responses/status.json');

        Http::fake([
            '*' => Http::response($content, 200, ['Content-Type' => 'application/json']),
        ]);

        $status = App::status();
        Http::assertSent(function (Request $request) {
            return $request->url() === sprintf(
                'https://%s.superoffice.com/api/state/%s',
                config('superoffice.environment'),
                config('superoffice.customer_id')
            ) &&
                $request->method() === 'GET';
        });

        $this->assertTrue($status['IsRunning']);
    }
}
