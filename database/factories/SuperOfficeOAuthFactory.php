<?php

namespace Cadix\SuperOfficeApi\Database\Factories;

use Cadix\SuperOfficeApi\Models\SuperOfficeOAuth;
use Cadix\SuperOfficeApi\Tests\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SuperOfficeOAuthFactory extends Factory
{
    protected $model = SuperOfficeOAuth::class;

    /**
     * @return array<string>
     */
    public function definition(): array
    {
        return [
            'access_token'        => Str::random(20),
            'refresh_token'       => Str::random(40),
            'superoffice_user_id' => $this->faker->unique(true)->randomNumber(),
            'user_id'             => function () {
                return User::factory()->create()->id;
            },
            'user_type' => get_class(( new User() )),
        ];
    }
}
